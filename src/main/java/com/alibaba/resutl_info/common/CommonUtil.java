package com.alibaba.resutl_info.common;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class CommonUtil {
    private static int result;
    private static ObjectMapper mapper;
    /**
     * 重写hashcode:提高性能
     * hashcode:本身有C++实现的返回的是对象存储的物理位置(不同对象的hashcode值也是会一样的)
     *优点：寻址十分快速，缺点：不能保证唯一
     * @param hashcode
     * @return
     */
    public static final int Hashcode(int hashcode){
        return ( result= hashcode) ^ (result >>> 16);
    }
    public static final String getCurrent(){
        return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
    }
    private static ObjectMapper getMapper(){
        return new ObjectMapper();
    }
    public static   <T> T parseObject(String str,Class<T> clazz){
        T t=null;
        try {
            getMapper().readValue(str, clazz);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return  t;
    }
    public static   <T> T readJSON(String URL,Class<T> o){
        T t = null;
        try {
            t = getMapper().readValue(new File(URL), o);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return  t;
    }
    public static boolean wirteJSON(String URL,Class o){
        try {
            getMapper().writeValue(new File(URL),o);
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

}
