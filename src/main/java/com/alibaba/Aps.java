package com.alibaba;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Aps {

	public static void main(String[] args) {
		SpringApplication.run(Aps.class, args);
	}
}
